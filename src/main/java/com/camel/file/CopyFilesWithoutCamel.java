package com.camel.file;

import java.io.*;

public class CopyFilesWithoutCamel {
    public static void main(String args[]){
        //Step 1: Create a file Object to read the directories
        File inputDirectory = new File("data/input");
        File outputDirectory = new File("data/output");
        //Step 2: Read the files from the directory.
        File[] files = inputDirectory.listFiles();
        //Step 3: Iterate the files.
        for (File source : files){
            if (source.isFile()){
                File destino = new File(
                        outputDirectory.getPath()
                        + File.separator
                        + source.getName()
                );
            //Step 4: Create the output stream.
                try {
                    OutputStream outputStream = new FileOutputStream(destino);
                    byte[] buffer = new byte[(int) source.length()];
                    FileInputStream inputStream = new FileInputStream(source);
                    inputStream.read(buffer);
                    try {
                        outputStream.write(buffer);
                    } catch (IOException exception) {
                        System.out.println("Error al escribir en el outputStream: " + exception.getMessage());
                    } finally { //Step 5: Close the Streams
                        outputStream.close();
                        inputStream.close();
                    }
                }catch (Exception exception){
                    System.out.println("Error al crear el outputStream: " + exception.getMessage());
                }
            }
        }


    }
}
